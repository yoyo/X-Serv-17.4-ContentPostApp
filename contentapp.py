#!/usr/bin/python3

"""
 contentApp class
 Simple web application for managing content

 Copyright Jesus M. Gonzalez-Barahona, Gregorio Robles 2009-2015
 jgb, grex @ gsyc.es
 TSAI, SAT and SARO subjects (Universidad Rey Juan Carlos)
 October 2009 - March 2015
"""

import webapp


#Formulario
formulario = """
    <form action="" method="post">                           
      Nombre de usuario: <br>
      <input type="text" name="usuario"><br><br>
      <input type="submit" value="Enviar datos">
    </form>
"""

class contentApp (webapp.webApp):
    """Simple web application for managing content.

    Content is stored in a dictionary, which is intialized
    with the web content."""

    # Declare and initialize content
    content = {'/': 'Root page',
               '/page': 'A page'
               }

    def parse(self, request):
        """Return the resource name (including /)"""

        #Metodo Recurso Cuerpo
        #GET   /pepito  (  )        <---------- Lectura
        #Post /pepito   valor  <------------- Escritura

        Metodo = request.split(' ', 2)[0]     #Nos quedamos con el tipo de metodo
        Recurso = request.split(' ', 2)[1]     #Nos quedamos con el recurso
        Cuerpo = request.split('\r\n\r\n', 1)[1]
        return (Metodo, Recurso, Cuerpo)

    def process(self, petition):
        """Process the relevant elements of the request.

        Finds the HTML text corresponding to the resource name,
        ignoring requests for resources not in the dictionary.
        """
        Metodo, Recurso, Cuerpo = petition  #El paso de funcion de parse a la del process

        if Metodo == "GET":
            if Recurso in self.content:
                httpCode = "200 OK"
                htmlBody = "<html><body><p>Recurso: " + Recurso  + \
                " encontrado.</p><p>Valor:" + self.content[Recurso] + \
                "</p><p>Opcion de actualizar su valor:</p>" + \
                 formulario + "</body></html>"

            else:
                httpCode = "404 Not Found"
                htmlBody = "</body></html>Not Found...Añadelo tu mismo" + \
                            formulario + "</body></html>"

            return (httpCode, htmlBody)

        if Metodo == "POST":   #Aqui debe ir cojo el cuerpo y el content : recurso ---> Cuerpo
            self.content[Recurso] = Cuerpo.split('=')[1]
            httpCode = "200 OK"
            htmlBody = "<html><body><p>Recurso: " + Recurso + \
            "</p><p>Valor Nuevo:" + self.content[Recurso] + \
            "</p><br><p>Puedes actualizar su valor:</p><br><br>" + \
            formulario + "</body></html>"

            return (httpCode, htmlBody)


if __name__ == "__main__":
    testWebApp = contentApp("localhost", 1234)
